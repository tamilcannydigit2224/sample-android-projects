package com.cannydigit.localdatabase

data class ContactData(
    var id: Int? = null,
    var name: String? = "",
    var mobile: String? = ""
)