package com.cannydigit.localdatabase

import android.content.ContentValues
import android.content.Context
import android.database.DatabaseUtils
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

private const val DATABASE_NAME = "Test.db"
private const val TABLE_NAME = "contacts"
private const val COLUMN_ID = "id"
private const val COLUMN_NAME = "name"
private const val COLUMN_MOBILE = "mobile"
class DataBaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, 1) {
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("create table contacts (id integer primary key autoIncrement not null, name text, mobile text)")
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS contacts")
        onCreate(db)
    }

    fun create(name: String, mobile: String) : Boolean {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COLUMN_NAME, name)
        contentValues.put(COLUMN_MOBILE, mobile)
        db.insert(TABLE_NAME, null, contentValues)
        return true
    }

    fun readContactById(id: Int) : ContactData {
        val db = this.readableDatabase
        val cursor = db.rawQuery("select * from contacts where id=$id", null)
        cursor.moveToFirst()
        val contact = ContactData()
        contact.id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID))
        contact.name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME))
        contact.mobile = cursor.getString(cursor.getColumnIndex(COLUMN_MOBILE))
        cursor.close()
        return contact
    }

    fun readAllContacts() : ArrayList<ContactData> {
        val contacts = ArrayList<ContactData>()
        val db = this.readableDatabase
        val cursor = db.rawQuery("select * from contacts", null)
        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            val contact = ContactData()
            contact.id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID))
            contact.name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME))
            contact.mobile = cursor.getString(cursor.getColumnIndex(COLUMN_MOBILE))
            contacts.add(contact)
            cursor.moveToNext()
        }
        if (cursor.isAfterLast) {
            cursor.close()
        }
        return contacts
    }

    fun updateContact(contactData: ContactData): Boolean {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COLUMN_NAME, contactData.name)
        contentValues.put(COLUMN_MOBILE, contactData.mobile)
        db.update(TABLE_NAME, contentValues, "id=?", arrayOf((contactData.id!!).toString()))
        return true
    }

    fun deleteContact(id: Int) {
        val db = this.writableDatabase
        db.delete(TABLE_NAME, "id=?", arrayOf((id).toString()))
//        db.execSQL("delete from $TABLE_NAME where id=?$id")
    }

    fun deleteAll() {
        val db = this.writableDatabase
        db.execSQL("delete from $TABLE_NAME")
    }

    fun numberOfContacts() : Long {
        val db = this.readableDatabase
        return DatabaseUtils.queryNumEntries(db, TABLE_NAME)
    }
}