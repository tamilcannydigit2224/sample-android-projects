package com.cannydigit.localdatabase

import android.os.Bundle
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var selectedContact = ContactData()
    private lateinit var dbSQL : DataBaseHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        defineViews()
    }

    private fun defineViews() {
        dbSQL = DataBaseHelper(this)
        showContactList()

        btn_save.setOnClickListener {
            dbSQL.create(getName(), getPhone())
            showContactList()
        }

        btn_update.setOnClickListener {
            if (dbSQL.updateContact(ContactData(selectedContact.id, getName(), getPhone()))) {
                showToast("UPDATED")
                showContactList()
            } else {
                showToast("Failed")
            }
        }

        btn_delete.setOnClickListener {
//            dbSQL.deleteAll()
            dbSQL.deleteContact(selectedContact.id!!)
//            Log.i("DELETE", "RESPONSE =>>>> ${dbSQL.deleteContact(selectedContact.id!!)}")
            showContactList()
        }
    }

    fun setData(contact: ContactData) {
        selectedContact = contact
        et_name.setText(contact.name)
        et_phone.setText(contact.mobile)
        manageButtonVisibility(true)
        /*val data = dbSQL.readContactById(contact.id!!)
        Toast.makeText(this, "Contact data \n $data", Toast.LENGTH_LONG).show()*/
    }

    private fun manageButtonVisibility(isEdit: Boolean) {
        if (isEdit) {
            btn_save.visibility = GONE
            btn_delete.visibility = VISIBLE
            btn_update.visibility = VISIBLE
        } else {
            btn_save.visibility = VISIBLE
            btn_delete.visibility = GONE
            btn_update.visibility = GONE
        }
    }

    private fun getName() : String {
        return et_name.text.toString().trim()
    }

    private fun getPhone() : String {
        return et_phone.text.toString().trim()
    }

    private fun showContactList() {
        manageButtonVisibility(false)
        val contacts = dbSQL.readAllContacts()
        tv_count.text = dbSQL.numberOfContacts().toString()
        rv_contacts.layoutManager = LinearLayoutManager(this)
        rv_contacts.adapter = ContactsAdapter(this, contacts)
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}