package com.cannydigit.localdatabase

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_contact.view.*

class ContactsAdapter(private val context: MainActivity, private val contacts: ArrayList<ContactData>) : RecyclerView.Adapter<ContactHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactHolder {
        return ContactHolder(LayoutInflater.from(context).inflate(R.layout.list_contact, parent, false))
    }

    override fun onBindViewHolder(holder: ContactHolder, position: Int) {
        val contact = contacts[position]
        holder.tvName.text = contact.name
        holder.tvMobile.text = contact.mobile
        holder.container.setOnClickListener {
            context.setData(contact)
        }
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

}

class ContactHolder(view: View) : RecyclerView.ViewHolder(view) {
    val tvName = view.tv_name!!
    val tvMobile = view.tv_mobile!!
    val container = view.cl_container!!
}